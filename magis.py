#! /usr/bin/env python3

# Copyright 2013 Solt Budavari

# magis is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# magis is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with magis.  If not, see <http://www.gnu.org/licenses/>.




import random



## ================
## Data definitions:


## Question is String
## Interp. Identifies the text to be recalled.
Q1 = "The Houses of Hogwarts:"
Q2 = "Roses are red,"

#def fn_for_question( q ):  # Template
#    return ... q



## Answer is (listof String)
## Interp. The lines of the text to be recalled.
A1 = ["Gryffindor", "Ravenclaw", "Hufflepuff", "Slytherin"]
A2 = ["Violets are blue,",
      "This poem sucks,",
      "And so do you."]

#def fn_for_answer( a ):  # Template
#    for line in a:
#        ... line
#    return ...



## Problem is Tuple(Question, Answer)
## Interp. A Question and the corresponding Answer.
P1 = (Q1, A1)
P2 = (Q2, A2)

#def fn_for_problem( p ):  # Template
#    fn_for_question( p[0] )
#    fn_for_answer( p[1] )
#    return ...



## Problem-Set is (listof Problem)
## Interp. A list of problems.
PS1 = [P1, P2]

#def fn_for_probset( ps ):  # Template
#    for prob in ps:
#        fn_for_problem( prob )
#    return ...




## ================
## Functions:

def main():
    problem_set = read_probs( open( get_filename(), "r" ) )
    test_probs( shuffle_probs( problem_set ) )
    
    return 0



def read_probs( inputfile ):
    """
    (File open for reading) -> (listof Problem)
    Create a Problem-Set by reading a well-formed Probem-Set file.
    """
    
    lop = []
    prob = []
    
    for line in inputfile:
        if line != "\n":
            prob.append( line.strip() )
        else:
            lop.append( ( prob[0], prob[1:] ) )
            prob = []
            
    if prob != []:
        lop.append( ( prob[0], list( prob[1:] ) ) )
    
    inputfile.close()
    
    return lop


def get_filename():
    """
    None -> String
    Prompt the user for the name of the fie to open.
    """
    
    return input( "Please enter the name of the file to open: " )



def test_probs( ps ):
    """
    Problem-set -> None
    Test the user with each Problem in ps.
    """
    
    for problem in ps:
        test_prob( problem )
    
    return



def shuffle_probs( ps ):
    """
    ProblemSet -> ProblemSet
    Produce a ProblemSet with same Problems as ps but in different order.
    """
    
    new_ps = []
    tmp = list( ps )
    
    while tmp != []:
        rnum = ( random.randint( 0, ( len( tmp )-1 ) ) )
        new_ps.append( tmp.pop( rnum ) )
    
    return new_ps



def test_prob( p ):
    """
    Problem -> None
    Test the user's knowledge with a problem.
    """
    
    print( p[0] )
    get_answers( p[1] )
    print( "" )
    
    return



def get_answers(a):
    """
    Answer -> None
    Read and check a line of input against each string in a; in case of mismatch, print correct string.
    """
    
    for line in a:
        UserAnswer = input().strip().lower()
        if UserAnswer != line.lower():
            print( line )
    
    return



if __name__ == "__main__":
    main()
