Fejfájást csillapító por
Rp. Metamizoli natrici
gma semis (0,50)
Coffeini
cgta quinque (0,05)
M. f. pulv.
Dent. tal. dos. no decem (x)
S. Fájdalom esetén egy por.
Naponta max. 3x

Simaizom-görcs elleni görcsoldó és fájdalomcsillapító paraszimpatolitikummal
Rp. Homatropini methylbromidi
mgta quinque (0,005)
Papaverini hydrochloridi
cgta quinque (0,05)
Metamizoli natrici
gma semis (0,50)
M. f. pulv.
Dent. tal. dos. no decem (x)
S. Fájdalom esetén egy por.
Naponta max. 3x

Simaizom-fájdalomban nem kábító fájdalomcsillapító és görcsoldó kúp
Rp. Papaverini hydrochloridi
cgta quinque (0,05)
Metamizoli natrici
gma semis (0,50)
Vehiculi
qu. s.
M. f. supp.
Dent. tal. dos. no decem (x)
S. Görcsös fájdalom esetén egy kúpot végbélbe helyezni.
Naponta max. 3x

Simaizomgörcs elleni görcsoldó és fájdalomcsillapító kúp paraszimpatolitikummal
Rp. Atropini sulfatis
mgma semis (0,0005)
Papaverini hydrochloridi
cgta decem (0,10)
Metamizoli natrici
gma semis (0,50)
Vehiculi
qu. s.
M. f. supp.
Dent. tal. dos. no decem (x)
S. Fájdalom esetén egy kúpot végbélbe helyezni.
Naponta max. 3x

Morfint és simaizomtónus-csökkentőt tartalmazó kúp
Rp. Morphini hydrochloridi
cgta duo (0,02)
Atropini sulfatis
mgma semis (0,0005)
Vehiculi
qu. s.
M. f. supp.
Dent. tal. dos. no decem (x)
S. Fájdalom esetén egy kúpot végbélbe helyezni.
Naponta max. 3x
Kábítószer, 2 példányban!

Láz- és köhögéscsillapító kanalas orvosság
Rp. Codeini hydrochloridi dihydrici
cgta viginti (0,20)
Metamizoli natrici
gta quinque (5,0)
Acidi ascorbici
gma unum (1,0)
Sirupi simplicis
gta quindecim (15,0)
Aquae purificatae
ad gta centum et quinquaginta (150,0)
M. f. sol.
Da in vitro
S. 3x1 evőkanállal naponta

Hörgtágítót, köhögéscsillapítót és köptetőt tartalmazó kanalas orvosság
Rp. Codeini hydrochloridi dihydrici
Ephedrini racemici hydrochloridi
ana cgta viginti (0,20)
Elixirii thymi compositi
ad gta centum et quinquaginta (150,0)
M. f. sol.
D. S. 3x1 evőkanállal naponta

Sósav- és pepszintartalmú savpótló kanalas orvosság
Rp. Acidi hydrochloridi diluti
gta viginti (20,0)
Pepsini pulveris
cgta viginti (0,20)
Aquae purificatae
ad gta trecenta (300,0)
M. f. sol.
D. S. Egy evőkanállal 1 pohár vízben, étkezés közben, szívószállal

Kábító fájdalomcsillapító ópium-alkaloid cseppek
Rp. Morphini hydrochloridi
cgta quadraginta (0,40)
Aquae purificatae
ad gta decem (10,0)
M. f. sol.
Da in vitro guttatorio
S. 4 óránként 10 csepp naponta
Kábítószer, 2 példányban!

Köhögéscsillapító cseppek
Rp. Codeini hydrochloridi dihydrici
cgta quadraginta (0,40)
Aquae purificatae
ad gta viginti (20,0)
M. f. sol.
Da in vitro cum pipetta
S. 3x20 csepp naponta

Hypotonia kezelésére cseppek
Rp. Ephedrini racemici hydrochloridi
cgta octoginta (0,80)
Aquae purificatae
ad gta viginti (20,0)
M. f. sol.
Da in vitro guttatorio
S. 2x20 cseppet naponta

Glaucomás betegnek szemcsepp
Rp. Pilocarpini hydrochloridi
cgta viginti (0,20)
Solutionis ophtalmicae cum benzalkonio
ad gta decem (10,0)
M. f. oculogutta
Da in vitro guttatorio
S. Szemcsepp, külsőleg

Helyi érzéstelenítő szemcsepp
Rp. Tetracaini hydrochloridi
cgta decem (0,10)
Solventis pro oculoguttis cum thiomersalo
ad gta decem (10,0)
M. f. oculogutta
Da in vitro guttatorio
S. Szemcsepp, külsőleg

Antibiotikus szemcsepp
Rp. Neomycini sulfatis
cgta quinque (0,05)
Natrii chloridi
cgta octo (0,08)
Aquae destillatae pro injectione
ad gta decem (10,0)
M. f. oculogutta
Da in vitro guttatorio
S. Szemcsepp, külsőleg

Pupillatágító szemcsepp
Rp. Atropini sulfatis
cgta decem (0,10)
Solutionis ophtalmicae cum benzalkonio
ad gta decem (10,0)
M. f. oculogutta
Da in vitro guttatorio
S. Szemcsepp, külsőleg

Blenorrhoea megelőzésére szemcsepp újszülötteknek
Rp. Argenti acetici
Kalii nitratis
ana cgta decem (0,10)
Aquae destillatae pro injectione
ad gta decem (10,0)
M. f. oculogutta
Da in vitro guttatorio
S. Az orvos kezéhez

Orrnyálkahártya-duzzanat ellen orcsepp
Rp. Ephedrini racemici hydrochloridi
cgta decem (0,10)
Solventis viscosae
ad gta decem (10,0)
M. f. nasogutta
Da in vitro guttatorio
S. Orrcsepp, külsőleg

Helyi érzéstelenítő kenőcs
Rp. Lidocaini
gta duo et semis (2,5)
Unguenti macrogoli
ad gta quinquaginta (50,0)
M. f. unguentum
D. S. Kenőcs, külsőleg

Keratolitikus kenőcs
Rp. Acidi salicylici
gta quinque (5,0)
Unguenti oleosi
ad gta quinquaginta (50,0)
M. f. unguentum
D. S. Kenőcs, külsőleg

Keratoplasztikus kenőcs
Rp. Acidi salicylici
gma unum et semis (1,5)
Unguenti oleosi
ad gta quinquaginta (50,0)
M. f. unguentum
D. S. Kenőcs, külsőleg

Helyi érzéstelenítő szuszpenzió
Rp. Benzocaini
gta tria (3,0)
Sirupi simplicis
Mucilaginis hydroxyaethylcellulosi
ana ad gta centum (100,0)
M. f. suspensio
D. S. 2-3 óránként 1 kávéskanálnyit szopogatva lenyelni

Sós hashajtó
Rp. Magnesii sulfatis heptahydrici
gta centum (100,0)
D. S. 2 kávéskanállal 2 dl vízbe tenni és meginni

Aphta ecsetelésére oldat
Rp. Acidi trichloracetici
gta tria (3,0)
Aquae purificatae
ad gta triginta (30,0)
M. f. sol.
D. S. Az orvos kezéhez!

Alkoholos, szalicilsavas fertőtlenítő oldat
Rp. Acidi salicylici
gta duo (2,0)
Alcoholi diluti 70%
ad gta centum (100,0)
M. f. sol.
D. S. Külsőleg

Candidiasisban borax-oldat
Rp. Boracis
gta quattuor (4,0)
Glyceroli 85%
ad gta viginti (20,0)
M. f. sol.
D. S. 1 kávéskanállal lassan elszopogatni

Hűsítő kenőcs
Rp. Aluminii acetici tartarici soluti
gta decem (10,0)
Aquae purificatae
gta triginta (30,0)
Unguenti simplicis
ad gta centum (100,0)
M. f. unguentum
D. S. Kenőcs, külsőleg
