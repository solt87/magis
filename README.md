magis
=====

Introduction
------------

magis is a simple software to help memorising data of short text form (e.g. poems, definitions).
It works by printing the first line of every unit of text, and then getting your input line by line,
comparing it with the expected lines and printing the latter if significant difference is found.

Installing magis
----------------

magis itself needs no installing, but it requires Python 3 to be installed.

Using magis
-----------

Run magis from command line by giving its name as an argument to Python 3.

Unix/Linux:

    $ python3 magis.py

Windows(?):

    c:\> python3.exe magis.py

### Using input files made by others ###

It is advisable to read any input files before using them, since this is the only way you can figure out
what answers are expected. By all means edit the files if the need arises.

### Creating your own input files ###

Input files are simple text files. Each piece of text without blank lines is
considered one unit of text. When testing, the first line of the current unit is printed, then
the following lines are expected as input (one line at a time, i.e. you have to press Enter after
each line of input).

To use your own input file, enter the filename when magis asks for it.
